package com.example.myapplication

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi

class MainActivity : AppCompatActivity(), View.OnClickListener {
     lateinit var buttonone : Button
    lateinit var buttontwo : Button
     lateinit var buttonthree : Button
     lateinit var buttonfour : Button
     lateinit var buttonfive : Button
     lateinit var buttonsix : Button
     lateinit var buttonseven : Button
     lateinit var buttoneight : Button
     lateinit var buttonnine : Button
     lateinit var reset : Button



     var player1 = 0
     var player2 = 1
     var activeplayer = player1
     lateinit var filledpos : intArray
     var gameactive = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        filledpos = intArray(-1,-1,-1,-1,-1,-1,-1,-1,-1)



        buttonone = findViewById(R.id.buttonone)
        buttontwo = findViewById(R.id.buttontwo)
        buttonthree = findViewById(R.id.buttonthree)
        buttonfour = findViewById(R.id.buttonfour)
        buttonfive = findViewById(R.id.buttonfive)
        buttonsix = findViewById(R.id.buttonsix)
        buttonseven = findViewById(R.id.buttonseven)
        buttoneight = findViewById(R.id.buttoneight)
        buttonnine = findViewById(R.id.buttonnine)
        reset = findViewById(R.id.reset)


        buttonone.setOnClickListener(this)
        buttontwo.setOnClickListener(this)
        buttonthree.setOnClickListener(this)
        buttonfour.setOnClickListener(this)
        buttonfive.setOnClickListener(this)
        buttonsix.setOnClickListener(this)
        buttonseven.setOnClickListener(this)
        buttoneight.setOnClickListener(this)
        buttonnine.setOnClickListener(this)
        reset.setOnClickListener(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onClick(v: View?) {

        if (!gameactive)
            return

        var btnClicked = findViewById<Button>(v!!.id)
        var clickedTag = Integer.parseInt(btnClicked.tag.toString())

        if (filledpos[clickedTag] != -1)
            return

        filledpos[clickedTag] = activeplayer

        if(activeplayer == player1) {
            btnClicked.setText("0")
            activeplayer = player2
            btnClicked.backgroundTintList = getColorStateList(R.color.orange)
        }else{
            btnClicked.setText("x")
            activeplayer = player1
            btnClicked.backgroundTintList = getColorStateList(R.color.red)
        }

        checkForWin()
    }

    private fun checkForWin() {
        var winPos = arrayOf(intArrayOf(1,2,3),intArrayOf(4,5,6),intArrayOf(7,8,9),intArrayOf(1,4,7),intArrayOf(2,5,8),intArrayOf(3,6,9),intArrayOf(1,5,9),intArrayOf(3,5,7))

        for (i in 0 until winPos.size){
            var val1 = winPos[i][1]
            var val2 = winPos[i][2]
            var val3 = winPos[i][3]
           if (filledpos[val1] == filledpos[val2] && filledpos[val2] == filledpos[val3]){
              if (filledpos[val1] != -1 ) {
                  gameactive = false

              }
               if (filledpos[val1] == player1){
                   Toast.makeText(this, "player-1 is winner", Toast.LENGTH_SHORT).show()
               }else{
                   Toast.makeText(this, "player-2 is winner", Toast.LENGTH_SHORT).show()
               }

           }






        }
    var count=0
    for (i in 0 until filledpos.size) {
        if (filledpos[i] == -1) {
            count++
        }
    }
    if(count==0){

        }



    }

@RequiresApi(Build.VERSION_CODES.M)
private fun reset() {
   filledpos = intArray(-1,-1,-1,-1,-1,-1,-1,-1,-1)
    activeplayer - player1
    gameactive = true
    buttonone.setText("")
    buttontwo.setText("")
    buttonthree.setText("")
    buttonfour.setText("")
    buttonfive.setText("")
    buttonsix.setText("")
    buttonseven.setText("")
    buttoneight.setText("")
    buttonnine.setText("")
    buttonone.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttontwo.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttonthree.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttonfour.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttonfive.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttonsix.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttonseven.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttoneight.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
    buttonnine.backgroundTintList = getColorStateList(R.color.design_default_color_primary)
}



}